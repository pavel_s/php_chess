<?php

include 'Chess/Board.php';
include 'Chess/BoardPosition.php';

include 'Chess/Figure.php';
include 'Chess/FigurePawn.php';
include 'Chess/FigureRook.php';

include 'Chess/Storage.php';
include 'Chess/StorageFile.php';
include 'Chess/StorageRedis.php';


$storage = new \Chess\StorageFile();
$board = new \Chess\Board($storage);

$pawn = new Chess\FigurePawn();
$rook = new Chess\FigureRook();

$board->addFigure($pawn, new Chess\BoardPosition(1, 2));
$board->addFigure($rook, new Chess\BoardPosition(2, 1));

$board->save();

$board2 = new \Chess\Board($storage);
$board2->load();
$board2->addFigure($rook, new Chess\BoardPosition(2, 3));