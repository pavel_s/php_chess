<?php

namespace Chess;

interface Storage
{

    public function save(string $str);

    public function load() : string;

}