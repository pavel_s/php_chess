<?php

namespace Chess;

class Board
{
    private $storage;
    private $figures = [];


    public function __construct(Storage $storage)
    {
        $this->storage = $storage;
    }


    public function addFigure(Figure $figure, BoardPosition $position)
    {
        if (array_key_exists($figure->getHash(), $this->figures)) {
            throw new \Exception('такая фигура уже есть');
        }
        $this->figures[$figure->getHash()] = $position;
        echo $figure->getMessageAfterAddingOnBoard().PHP_EOL;
    }


    public function moveFigure(Figure $figure, BoardPosition $position)
    {
        if (!array_key_exists($figure->getHash(), $this->figures)) {
            throw new \Exception('такой фигуры нет на доске');
        }
        $this->figures[$figure->getHash()] = $position;
    }


    public function save()
    {
        $strFigures = serialize($this->figures);
        $this->storage->save($strFigures);
    }


    public function load()
    {
        $strFigures = $this->storage->load();
        $this->figures = unserialize($strFigures);
    }


}