<?php

namespace Chess;

class FigureRook extends Figure
{

    public function getMessageAfterAddingOnBoard() : string
    {
        return 'добавление ладьи на доску';
    }

}