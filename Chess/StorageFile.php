<?php

namespace Chess;

class StorageFile implements Storage
{
    const fileName = 'board.txt';

    public function save(string $str)
    {
        file_put_contents(self::fileName, $str);
    }

    public function load() : string
    {
        return file_get_contents(self::fileName);
    }

}