<?php

namespace Chess;

abstract class Figure
{

    public function getMessageAfterAddingOnBoard() : string
    {
        return 'добавление фигуры на доску';
    }

    public function getHash()
    {
        return spl_object_hash($this);
    }

}